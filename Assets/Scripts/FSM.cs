using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FSM  : MonoBehaviour {
	public event System.Action<FSM, FSM.StateI, FSM.StateI> OnStateChanged;

	public StateI currentState { private set; get;}

	public interface StateI {
		FSM fsm{ get; set;}

		void Execute();
		void FixedExecute();
		void Exit();
	}

	public interface State0ParamsI               : StateI { void Enter();}
	public interface State1ParamI< T >           : StateI { void Enter(T p);}
	public interface State2ParamsI< T0, T1 >     : StateI { void Enter(T0 p0, T1 p1); }
	public interface State3ParamsI< T0, T1, T2 > : StateI { void Enter(T0 p0, T1 p1, T2 p2); }

	[System.Serializable]
	public class StateBase : StateI {
		[HideInInspector]
		public FSM fsm { get; set;}
		
		public virtual void Execute      () {}
		public virtual void FixedExecute () {}
		public virtual void Exit         () {}
	}

	[System.Serializable] public class State                               : StateBase, State0ParamsI               { public virtual  void Enter(){}}
	[System.Serializable] public abstract class State1Param< T >           : StateBase, State1ParamI < T >          { public abstract void Enter(T p);}
	[System.Serializable] public abstract class State2Params< T0, T1 >     : StateBase, State2ParamsI< T0, T1 >     { public abstract void Enter(T0 p0, T1 p1); }
	[System.Serializable] public abstract class State3Params< T0, T1, T2 > : StateBase, State3ParamsI< T0, T1, T2 > { public abstract void Enter(T0 p0, T1 p1, T2 p2); }

	bool quitting = false;
	void OnApplicationQuit() {
		quitting = true;
	}

	public void OnDestroy ()
	{
		quitting |= !SceneManager.GetSceneAt(SceneManager.sceneCount - 1).isLoaded; 
		if (currentState != null && !quitting) {
			currentState.Exit();
		}
	}
	
	public void  Update() {
		if (currentState != null) 
			currentState.Execute();
	}
	
	public void FixedUpdate() {
		if(currentState != null)
			currentState.FixedExecute();
	}
	
	int stateChange = 0;
	private bool ChangeStateBase(StateI _newState) {
		int cachedStateChanged = ++ stateChange;
		
		StateI prevState = currentState;
		if (currentState != null) {
			currentState.Exit();
			if(stateChange != cachedStateChanged) 
				return false;
		}
		
		currentState = _newState;
		
		if (currentState != null) {
			currentState.fsm = this;

			if (OnStateChanged != null) {
				OnStateChanged (this, prevState, _newState);
				if(stateChange != cachedStateChanged) 
					return false;
			}

			return true;
		}
		return false;
	}
	
	public void  ChangeState(State0ParamsI _newState) {
		if(ChangeStateBase(_newState)) {
			_newState.Enter();
		}
	}
	
	public void  ChangeState< P >(State1ParamI< P > _newState, P _param) {
		if(ChangeStateBase(_newState)) {
			_newState.Enter(_param);
		}
	}
	
	public void  ChangeState< P0, P1 >(State2ParamsI< P0, P1 > _newState, P0 _param0, P1 _param1) {
		if(ChangeStateBase(_newState)) {
			_newState.Enter(_param0, _param1);
		}
	}

	public void  ChangeState< P0, P1, P2 >(State3ParamsI< P0, P1, P2 > _newState, P0 _param0, P1 _param1, P2 _param2) {
		if(ChangeStateBase(_newState)) {
			_newState.Enter(_param0, _param1, _param2);
		}
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(FSM))]
public class FSMEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		if ((target as FSM).currentState != null) {
			GUI.color = Color.yellow;
			GUILayout.Label("Current state: " + (target as FSM).currentState.ToString());	
		}
		Repaint();
	}
} 

#endif
