﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIButtonClick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

	public static event System.Action< string, UIButtonClick > onAnyButtonClick;
	public event System.Action< string, UIButtonClick > onClick;
	[SerializeField]private string id;
	public string Id {
		get {
			return string.IsNullOrEmpty(id) ? gameObject.name : id;
		}
		set {
			id = value;
		}
	}

	public bool mute = false;
	public AudioClip soundEventClick;

	public float feedbackDelay = 0.0f;

	private bool waitingDelay = false;
	private bool ignoreClick;
	private Button button;
	
	void Start() {
		if (string.IsNullOrEmpty (id)) {
			id = gameObject.name;
		}
		button = GetComponent<Button> ();

		if (transform.parent != null)
		{
			var dragableObject = (MonoBehaviour)transform.parent.GetComponentInParent<IDragHandler>();
			if (dragableObject != null)
			{
				gameObject.AddComponent<UIDragPasser>().handler = dragableObject;
			}
		}
		
	}

	public void OnPointerDown(PointerEventData eventData) {
		ignoreClick = false;
	}

	public void OnPointerUp(PointerEventData eventData) {
		if (!ignoreClick && waitingDelay == false) {

			if (button == null || button.interactable) {

				System.Action notify = () => {
					onAnyButtonClick.SafeInvoke(id, this);
					onClick.SafeInvoke(id, this);					
				};

				if (feedbackDelay > 0) {
					waitingDelay = true;
					this.DoAfterSeconds(feedbackDelay, () => {
						waitingDelay = false;
						notify();
					});
				} else {
					notify();
				}

				if (!mute) {
					SoundManager.Play(soundEventClick);
				}
			}
		}
	}

	public void OnDrag(PointerEventData data) {
		ignoreClick = true;
	}

	public static bool HasZeroSubscribers() {
		bool ret = onAnyButtonClick == null || onAnyButtonClick.GetInvocationList ().Length == 0;

		//Until we find a way to detect if an object has been marked as DontDestroyOnLoad we need to do this ugly thing for objects that won't be destroyed...
		if (!ret && onAnyButtonClick.GetInvocationList ().Length != 0) {
			ret = true;
			foreach (var v in onAnyButtonClick.GetInvocationList ()) {
				Debug.LogError(v.Target.GetType() + " has static button subscriptions");
				ret = false;
				break;
			}
		}

		//Uncomment this to trace the subscribers
		//if(!ret) {
		//	onAnyButtonClick.SafeInvoke("?", null);
		//}

		return ret;
	}
}
