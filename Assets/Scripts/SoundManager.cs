﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	private AudioSource audioSource;

	[System.Serializable]
	public class SoundSet {
		public string id = "SoundSet";
		public AudioClip[] sounds;
	}

	private static SoundManager instance;
	private static SoundManager Instance{
		get {
			if (instance == null) {
				instance = (new GameObject ("SoundManager")).AddComponent< SoundManager >();
				instance.audioSource = instance.gameObject.AddComponent< AudioSource >();
				Volume = PlayerPrefs.GetFloat ("FxVolume", 1.0f);

				DontDestroyOnLoad(instance.gameObject);
			}
			return instance;
		}
	}

	public static void Play(AudioClip _clip) {
		if (_clip != null) {
			Instance.audioSource.PlayOneShot (_clip, 1.0f);
		}
	}

	public static void Play(SoundSet _clips) {
		if (_clips.sounds.Length == 0) {
			Debug.LogWarning ("Empty soundset " + _clips.id);
			return;
		}
		
		Instance.audioSource.PlayOneShot (_clips.sounds [Random.Range (0, _clips.sounds.Length)], 1.0f);
	}

	public static float Volume {
		get {
			return AudioListener.volume;
		}
		set {
			AudioListener.volume = value;
		}
	}
}
