﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Popups : MonoBehaviour
{
    FSM fsm;
    public TestState0 state0;
    public TestState1 state1;

    void Start() {
        fsm = gameObject.AddComponent< FSM >();
        fsm.ChangeState(state0);
    }
}
