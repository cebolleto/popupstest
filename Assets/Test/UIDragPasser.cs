﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIDragPasser : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public MonoBehaviour handler;

	public void OnBeginDrag(PointerEventData eventData) 
	{
		if (handler != null) {
			if (handler is IBeginDragHandler) {
				(handler as IBeginDragHandler).OnBeginDrag (eventData);
			}else {
				Debug.LogError ("Handler does not implements interface IBeginDragHandler");
			}
		}
	}

	public void OnDrag(PointerEventData eventData) 
	{
		if (handler != null) {
			if (handler is IDragHandler) {
				(handler as IDragHandler).OnDrag (eventData);
			}else {
				Debug.LogError ("Handler does not implements interface IDragHandler");
			}
		}
	}

	public void OnEndDrag(PointerEventData eventData) 
	{
		if (handler != null) {
			if (handler is IEndDragHandler) {
				(handler as IEndDragHandler).OnEndDrag (eventData);
			}else {
				Debug.LogError ("Handler does not implements interface IEndDragHandler");
			}
		}
	}
}
