﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OkPopup : Popup
{
    public Text message;

    public override void OnPopupPanelButtonClick(string buttonTag, PopupButton popupButton) {
        if(buttonTag == "ok") {
            PopupManager.Instance.Done(this, () => {});
        }
    }

    public static void Show(string _message) {
        OkPopup popup = (OkPopup)PopupManager.CreatePopupFromName("OkPopup");
        popup.message.text = _message;
        
        PopupManager.Instance.Show(popup);
    }
}
