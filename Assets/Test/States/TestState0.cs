﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TestState0 : FSM.State {
    public GameObject node;

    public override void Enter() {
        UIButtonClick.onAnyButtonClick += OnButtonClicked;
        node.SetActive(true);
    }
    
    public override void Exit() {
        UIButtonClick.onAnyButtonClick -= OnButtonClicked;
        node.SetActive(false);
    }
    
    void ShowTestPopup(int _idx) {
        TestPopup.Show("Popup " + _idx, 
            () =>{
                OkPopup.Show("Ok");
            }, 
            () => {
                ShowTestPopup(_idx + 1);
            }
        );
    }
    
    public void OnButtonClicked(string _id, UIButtonClick _button) {
        if(_id == "showPopup") {
            ShowTestPopup(0);
        } else if(_id == "changeToState1") {
            fsm.ChangeState(fsm.GetComponent< Popups >().state1);
        }
    }
}
