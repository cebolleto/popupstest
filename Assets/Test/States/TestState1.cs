﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TestState1 : FSM.State {
    public GameObject node;
    
    public override void Enter() {
        UIButtonClick.onAnyButtonClick += OnButtonClicked;
        node.SetActive(true);
    }
    
    public override void Exit() {
        UIButtonClick.onAnyButtonClick -= OnButtonClicked;
        node.SetActive(false);
    }
    
    public void OnButtonClicked(string _id, UIButtonClick _button) {
        if(_id == "back") {
            fsm.ChangeState(fsm.GetComponent< Popups >().state0);
        }
    }
}
