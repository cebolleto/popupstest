﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestPopup : Popup {
    public Text text; 

    System.Action onOk;
    System.Action onNext;

    public override void OnPopupPanelButtonClick(string buttonTag, PopupButton popupButton) {
        if(buttonTag == "ok") {
            onOk?.Invoke();
        } else if(buttonTag == "next") {
            onNext?.Invoke();
        } else if(buttonTag == "close") {
            PopupManager.Instance.Done(this, () => {});
        }
    }
    
    public static void Show(string _text, System.Action _onOk, System.Action _onNext) {
        TestPopup popup = (TestPopup)PopupManager.CreatePopupFromName("TestPopup");
        popup.text.text = _text;
        popup.onOk = _onOk;
        popup.onNext = _onNext;
        
        PopupManager.Instance.Show(popup);
    }
}
