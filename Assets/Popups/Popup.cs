﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animation))]
public class Popup : MonoBehaviour, IAnimationClipSource
{
	public event System.Action<Popup> OnAnimateIn;
	public event System.Action<Popup> OnAnimateOut;
	
	protected const string SHOW_ANIM_NAME = "Show";
	protected const string HIDE_ANIM_NAME = "Hide";
	
	public enum State {
		Hidden,
		AnimatingIn,
		Idle,
		AnimatingOut
	};
	public State state {private set; get;}
	[HideInInspector]public bool destroyAfterAnim;

	public PopupManager.Layer activeLayer = PopupManager.Layer.Default;
	public bool closeOnBackgroundClicked = true;
	public bool showOverPrevious;
	public bool enqueue = false;

	public AnimationClip showAnimation;
	public AnimationClip hideAnimation;

	public int frameCompleted = -1; //Frame from which the animation can be consider completed (-1 means at 100%)

	protected new Animation animation;

	public virtual void OnPopupPanelButtonClick(string buttonTag, PopupButton popupButton) {}

	public void Show() {
		if(state == Popup.State.Hidden || state == Popup.State.AnimatingOut) {
			gameObject.SetActive (true);
			state = Popup.State.AnimatingIn;
			AnimateIn (() => {
				state = Popup.State.Idle;
			});
		}
	}

	protected virtual void AnimateIn (System.Action _onDone) {
		if (showAnimation == null) {
			showAnimation = Resources.Load("PopupIn") as AnimationClip;
		} 
		
		if (animation == null) {
			animation = gameObject.GetOrAddComponent<Animation>();
			animation.playAutomatically = false;
			animation.AddClip(showAnimation, SHOW_ANIM_NAME);
			if (hideAnimation != null) {
				animation.AddClip(hideAnimation, HIDE_ANIM_NAME);	
			}
		}
		
		OnAnimateIn.SafeInvoke(this);
		this.DoAfterAnimation (SHOW_ANIM_NAME, 1.0f, _onDone);
	}

	private bool EntryAnimOver() {
		return (!animation.isPlaying || (frameCompleted != -1 && animation[SHOW_ANIM_NAME].time * animation[SHOW_ANIM_NAME].clip.frameRate >= frameCompleted));
	}
	
	public void Hide(System.Action _onDone) {
		System.Action onHidden = () => {
			if (destroyAfterAnim) {
				GameObject.Destroy (gameObject);
			} else {
				gameObject.SetActive(false);
			}
			
			_onDone.SafeInvoke();
		};
	
		if(state == State.Hidden) {
			onHidden();
		} else {
			state = Popup.State.AnimatingOut;
			AnimateOut (() => {
				state = Popup.State.Hidden;
				onHidden();
			});
		}
	}

	protected virtual void AnimateOut(System.Action _onDone) {
		
		OnAnimateOut.SafeInvoke(this);
		if (animation == null) {
			_onDone ();
		} else {
			if (!EntryAnimOver() || hideAnimation == null) {
				this.DoAfterAnimation (SHOW_ANIM_NAME, -1.0f, _onDone);
			}
			else {
				this.DoAfterAnimation (HIDE_ANIM_NAME, 1.0f, _onDone);
			}
		}
	}
	
	public virtual float ExitTime() {
		if (animation == null) {
			return 0.0f;
		} else {
			if (!EntryAnimOver() || hideAnimation == null) {
				return !EntryAnimOver() ? animation[SHOW_ANIM_NAME].time : animation[SHOW_ANIM_NAME].length;
			}
			else {
				return animation[HIDE_ANIM_NAME].length;
			}
		}
	}
	
	public void GetAnimationClips(List<AnimationClip> results)
	{
		results.Add(showAnimation);
		results.Add(hideAnimation);
	}
}
