﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PopupButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler 
{
	public static event System.Action<PopupButton> OnAnyPopupButtonClick;

	public string id;

	public float feedbackDelay = 0.0f;
	public bool animated = true;

	public bool mute = false;
	public AudioClip soundEventClick;

	private bool ignoreClick;
	private Button button;
	private bool waitingDelay = false;
	private Popup popup;

	public Popup Popup
	{
		get {
			return popup;
		}
	}


	void Start() {
		if (string.IsNullOrEmpty (id)) {
			id = gameObject.name;
		}
		button = GetComponent< Button > ();
		popup = GetComponentInParent< Popup > ();

		/*if (id.Equals ("close")) {
			soundEventClick = SoundEvents.UI_BUTTON_CLOSE;
		}*/
	}

	public void OnPointerDown(PointerEventData eventData) {
		ignoreClick = false;
	}

	public void OnPointerUp(PointerEventData eventData) {
		if ((popup.state == Popup.State.Idle || Popup.state == Popup.State.AnimatingIn) && (PopupManager.Instance.frontPopup == popup) && !ignoreClick && (button == null || button.interactable) && waitingDelay == false) {

			System.Action notify = ()=>{

				if(!mute) {
					SoundManager.Play(soundEventClick);
				}

				popup.OnPopupPanelButtonClick (id, this);
				OnAnyPopupButtonClick?.Invoke(this);
			};

			if (feedbackDelay > 0.0f) {
				waitingDelay = true;
				this.DoAfterSeconds(feedbackDelay, () => {
					waitingDelay = false;
					notify();
				});
			} else {
				notify();
			}
		}
	}

	public void OnDrag(PointerEventData eventData) {
		ignoreClick = true;
	}
}
