﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;
using DG.Tweening.Core;

using UnityEngine.UI;

public class PopupManager : MonoBehaviour {

	public event System.Action<Popup> OnPopupShow;
	public event System.Action<Popup> OnPopupHide;
	public event System.Action<Popup> OnFrontPopupchanged;
	public event System.Action<bool> OnShowingPopups;

	public PopupBG bg;
	public Popup frontPopup { private set; get;}

	private List< Popup > popups = new List< Popup >();
	private List< Popup > activePopups = new List< Popup >();
	private TweenerCore< float, float, DG.Tweening.Plugins.Options.FloatOptions > bgTween;

	public bool IsShowingPopups {
		get {
			return bg.gameObject.activeSelf;
		}
	}
	
	public enum Layer {
		Default = 1 << 0,
		MapIdle = 1 << 1,
		Layer2  = 1 << 2,
		Layer3  = 1 << 3
	}
	[EnumMask]public Layer activeLayers = (Layer)(-1);
	
	
#region singleton
	private static PopupManager instance;
	public static PopupManager Instance {
		get {
			if(instance == null) {
				instance = GameObject.FindObjectOfType<PopupManager>();
			}
			return instance;
		}
	}

	public void OnDestroy() {
		instance = null;
	}
#endregion

	public bool CanPopupBeShown(Popup _popup) {
		return ((int)_popup.activeLayer & (int)activeLayers) != 0;
	}

	public void Show(Popup _popUp) {
		_popUp.gameObject.SetActive(false);
		popups.Insert(_popUp.enqueue ? 0 : activePopups.Count, _popUp);
		if(CanPopupBeShown(_popUp)) {
			activePopups.Insert(_popUp.enqueue ? 0 : activePopups.Count, _popUp);
		}
		
		ShowFront ();
		SortBG();
	}
	
	private void SortBG() {
		if(frontPopup != null) {
			bg.transform.SetSiblingIndex(Mathf.Max(0, frontPopup.transform.GetSiblingIndex() - 1));
		}
	}

	private void ShowFront() {
		if(activePopups.Count > 0 && frontPopup != activePopups[activePopups.Count - 1]) {
			if (frontPopup == null || activePopups [activePopups.Count - 1].showOverPrevious) {
				frontPopup = activePopups [activePopups.Count - 1];
				
				if ((frontPopup.state == Popup.State.AnimatingOut && !frontPopup.destroyAfterAnim) || (frontPopup.state == Popup.State.Hidden)) {
					OnPopupShow.SafeInvoke(frontPopup);
					frontPopup.Show();
					for(int i = activePopups.Count - 1; i >= 1 && activePopups[i].showOverPrevious; --i) {
						activePopups[i -1].Show();
					}
					
					if(!bg.gameObject.activeSelf || bg.GetComponentInChildren< Animation >()["PopupBGIn"].speed < 0.0f) {
						if(!bg.gameObject.activeSelf ) {
							OnShowingPopups.SafeInvoke(true);
						}

						bg.gameObject.SetActive (true);
						bg.DoAfterAnimation ("PopupBGIn", 1.0f, () => {});
					}
				}
			} else {
				Hide (frontPopup, () => {});
			}
		}
		OnFrontPopupchanged.SafeInvoke(frontPopup);
	}

	private void Hide(Popup _popup, System.Action _onDone) {
		if (activePopups.Count == 0) {
			bg.DoAfterAnimation ("PopupBGIn", -1.0f, () => {
				OnShowingPopups.SafeInvoke(false);
				bg.gameObject.SetActive (false);
			}, Mathf.Max(0.0f, _popup.ExitTime() - bg.GetComponentInChildren< Animation >(true)["PopupBGIn"].length));
		}

		_popup.Hide(() => {
			SortBG();
			OnPopupHide.SafeInvoke(_popup);
		});
		
		//Do not wait until animation is over to show the next popup
		if(frontPopup == _popup) {
			frontPopup = null;
		}
		_onDone.SafeInvoke ();
		ShowFront ();
	}

	public void Done(Popup _popup, System.Action _onDone) {
		popups.Remove(_popup);
		activePopups.Remove (_popup);
		_popup.destroyAfterAnim = true;
		
		Hide (_popup, _onDone);
	}
	
	protected virtual void Update() {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			if(frontPopup != null && frontPopup.state == Popup.State.Idle && frontPopup.closeOnBackgroundClicked) {
				frontPopup.OnPopupPanelButtonClick("close", null);
			}
		}
	}
	
	
	public void AddActiveLayer(Layer _layer) {
		SetActiveLayers((int)activeLayers | ((int)_layer));
	}
	
	public void RemoveActiveLayer(Layer _layer) {
		SetActiveLayers((int)activeLayers & ~((int)_layer));
	}
	
	public void SetActiveLayers(int _layers) {
		activeLayers = (Layer)_layers;
		activePopups.Clear();
		foreach (Popup p in popups) {
			if(CanPopupBeShown(p)) {
				activePopups.Add(p);
			} else {
				if(p.state == Popup.State.AnimatingIn || p.state == Popup.State.Idle) {
					Hide(p, () => {});
				}
			}
		}
		
		ShowFront();
		SortBG();
	}

    public int GetNumPopups(bool _includeInactive = false){
        return (_includeInactive) ? popups.Count : activePopups.Count;
    }

    private void OnValidate() {
		if(Application.isPlaying){
			SetActiveLayers((int)activeLayers);
		}
	}

	public static Popup CreatePopupFromName(string name) {
		GameObject prefabGO = Resources.Load("Popups/" + name) as GameObject;
		if(prefabGO == null){
			Debug.LogError("[FrozenPopups] Popup not found for key: " + name);
			return null;
		}

		return CreatePopupFromPrefab (prefabGO);
	}

	public static Popup CreatePopupFromPrefab (GameObject _prefabGO)
	{
		Popup popup = (Instantiate(_prefabGO, Instance.transform) as GameObject).GetComponent< Popup >();
		popup.transform.localPosition = Vector3.zero;
		popup.transform.localScale = Vector3.one;
		popup.gameObject.name = _prefabGO.name;

		return popup;
	}
}
