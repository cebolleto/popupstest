﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PopupBG : MonoBehaviour, IPointerClickHandler {
	public void OnPointerClick(PointerEventData eventData) {
		Popup popup = PopupManager.Instance.frontPopup;
		if (popup != null && popup.state == Popup.State.Idle && popup.closeOnBackgroundClicked) {
			popup.OnPopupPanelButtonClick("close", null);
		}
	}
}
