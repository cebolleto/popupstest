﻿using System;
using UnityEngine;

public class ToscanaPopup : Popup
{
    public enum UITrigger {
        ShowAll,
        ShowOnlyTop,
        ShowOnlyCoins,
        ShowNothing
    }
    public UITrigger uiTrigger = UITrigger.ShowNothing;
}
