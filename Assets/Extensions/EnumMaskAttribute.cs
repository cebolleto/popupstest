﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;
#endif

public class EnumMaskAttribute : PropertyAttribute {}
 
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(EnumMaskAttribute))]
    public class EnumFlagDrawer : PropertyDrawer
    {
      public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
      {
          EditorGUI.BeginProperty(position, label, property);
            property.intValue = EditorGUI.MaskField(position, label, property.intValue, Enum.GetNames(fieldInfo.FieldType).Where(_i => _i != "Nothing" && _i != "Everything").ToArray());
          EditorGUI.EndProperty();
      }
    }
#endif