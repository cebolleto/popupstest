﻿using UnityEngine;
using System.Collections;


public static class MonoBehaviorExtensions
{
	public static void DoAfterCondition(this MonoBehaviour _monoBehaviour, System.Func<bool> _predicate, System.Action _action)
	{
		_monoBehaviour.StartCoroutine(DoAfterCondition(_predicate, _action));
	}
	
	private static IEnumerator DoAfterCondition (System.Func<bool> _predicate, System.Action _action)
	{
		yield return new WaitUntil(_predicate);
		_action.SafeInvoke();
	}
	
	
	public static void DoAfterSeconds (this MonoBehaviour _monoBehavior, float _seconds, System.Action _action)
	{
		if(_seconds > 0)
			_monoBehavior.StartCoroutine(DoAfterSeconds(_seconds, _action));
		else
		{
			if (_action != null) {
				_action();
			}
		}
	}

	private static IEnumerator DoAfterSeconds (float _seconds, System.Action _action)
	{
		if(_seconds > 0)
			yield return new WaitForSeconds(_seconds);
		if (_action != null) {
			_action();
		}
	}
	
	public static void DoOnNextFrame (this MonoBehaviour _monoBehavior, int numFrame, System.Action _action)
	{
		_monoBehavior.StartCoroutine(DoOnNextFrame(numFrame, _action));
	}
	
	public static void DoOnEndOfFrame (this MonoBehaviour _monoBehavior, System.Action _action)
	{
		_monoBehavior.StartCoroutine(DoOnEndOfFrame(_action));
	}

	public static void DoOnNextFrame (this MonoBehaviour _monoBehavior, System.Action _action)
	{
		_monoBehavior.StartCoroutine(DoOnNextFrame(1, _action));
	}
	
	public static void DoOnNextFixedUpdate (this MonoBehaviour _monoBehavior, System.Action _action)
	{
		_monoBehavior.StartCoroutine(DoOnNextFixedUpdate(_action));
	}
	
	private static IEnumerator DoOnEndOfFrame (System.Action _action)
	{
		yield return new WaitForEndOfFrame();
		
		if (_action != null) {
			_action();
		}
	}

	private static IEnumerator DoOnNextFrame (int frames, System.Action _action)
	{
		for (int i = 0; i < frames; i++)
			yield return null;
		
		if (_action != null) {
			_action();
		}
	}
	
	private static IEnumerator DoOnNextFixedUpdate (System.Action _action)
	{
		yield return new WaitForFixedUpdate();
		if (_action != null) {
			_action();
		}
	}

	
	private static IEnumerator WaitAnimDone (Animation _anim, System.Action _onDone) {
		while (_anim.isPlaying) {
			yield return null;
		}

		_onDone.SafeInvoke ();
	}

	public static void DoAfterAnimation(this MonoBehaviour _monoBehavior, string _animName, float _speed, System.Action _onDone, float _delay = 0.0f) {
		DoAfterAnimation (_monoBehavior, _monoBehavior.GetComponentInChildren< Animation > (),_animName, _speed, _onDone, _delay);
	}

	public static void DoAfterAnimation(this MonoBehaviour _monoBehavior, Animation _anim, string _animName, float _speed, System.Action _onDone, float _delay = 0.0f) {
		if (_anim != null) {
			_anim [_animName].speed = _speed;
			if (!_anim.IsPlaying (_animName)) {
				_anim [_animName].time = (_speed > 0.0f) ? (0.0f - _delay) : (_anim [_animName].length + _delay);
				_anim.Play (_animName);
				_anim.Sample(); //this sample fixes an issue where the animation is not played until the next frame showing the popups opened during one moment 
			}

			_monoBehavior.StopAllCoroutines ();
			_monoBehavior.StartCoroutine (WaitAnimDone(_anim, _onDone));
		} else {
			Debug.LogError ("Animation " + _animName + " not found in " + _monoBehavior.gameObject.name);
			_onDone ();
		}
	}
}


