﻿using UnityEngine;
using System.Collections;

public static class GameObjectExtensions2 {

	public static GameObject InstantiateOnParent(this GameObject _go, Transform _parent)
	{
		GameObject go = GameObject.Instantiate(_go, _parent) as GameObject;
		go.transform.PositionOnParent(_parent);
		return go;
	}

	private static Bounds? GetGOBounds(this GameObject _go) {
		Bounds? bounds = null;

		Renderer mesh = _go.GetComponent< Renderer >();
		if(mesh != null && (mesh.GetType() == typeof(MeshRenderer) || mesh.GetType() == typeof(SkinnedMeshRenderer) || mesh.GetType() == typeof(SpriteRenderer))) {
			bounds = mesh.bounds;
		}

		RectTransform rt = _go.GetComponent< RectTransform >();
		if(rt != null) {
			Vector3[] corners = new Vector3[4];
			rt.GetWorldCorners(corners);
			Vector3 min = corners[0];
			Vector3 max = corners[0];

			for (int i = 1; i < corners.Length; ++i) {
				min = Vector3.Min (min, corners [i]);
				max = Vector3.Max (max, corners [i]);
			}

			Bounds rtBounds = new Bounds();
			rtBounds.SetMinMax (min, max);
			if (rtBounds.size != Vector3.zero) {
				bounds = rtBounds;
			}
		}

		if(mesh == null && rt == null) {
			Collider c = _go.GetComponent< Collider >();
			if(c != null) {
				bounds = c.bounds;
			}
		}

		return bounds;
	}

	public static Bounds GetBounds(this GameObject _thisgo, bool recursive = true) {
		Bounds? b = null;
		if (recursive) {
			DoRecursively (_thisgo, (_go) => {
				if(!_go.activeInHierarchy || _go.GetComponentInParent< IgnoreBounds >() != null)
					return;

				Bounds? _goBounds = GetGOBounds (_go);

				if (_goBounds.HasValue) {
					if (b.HasValue) {
						Bounds tmp = b.Value;
						tmp.Encapsulate (_goBounds.Value);
						b = tmp;
					} else {
						b = _goBounds.Value;
					}
				}
			});
		} else {
			b = GetGOBounds (_thisgo);
		}

		return b.HasValue ? b.Value : new Bounds(Vector3.zero, Vector3.zero);
	}

	public static void ClampInsideViewport(this GameObject _obj, Camera _cam, float minX = 0.0f, float maxX = 1.0f, float minY = 0.0f, float maxY = 1.0f) {
		RectTransform rectTransform = _obj.GetComponent<RectTransform> ();
		Bounds bounds = _obj.GetBounds ();

		Vector3 minPos = _cam.ViewportToWorldPoint (new Vector3 (minX, minY, 0));
		Vector3 maxPos = _cam.ViewportToWorldPoint (new Vector3 (maxX, maxY, 0));
		Vector3 clampedPos = _obj.transform.position;

		Vector3 scale = rectTransform.lossyScale;

		clampedPos.x = Mathf.Max (clampedPos.x, minPos.x + bounds.size.x * rectTransform.pivot.x);
		clampedPos.x = Mathf.Min (clampedPos.x, maxPos.x - bounds.size.x * (1f - rectTransform.pivot.x));

		clampedPos.y = Mathf.Max (clampedPos.y, minPos.y + bounds.size.y * rectTransform.pivot.y);
		clampedPos.y = Mathf.Min (clampedPos.y, maxPos.y - bounds.size.y * (1f - rectTransform.pivot.y));

		_obj.transform.position = clampedPos;
	}
	
	public static Bounds GetLocalBounds(this GameObject _obj) {
		//Cache current position and rotation
		Transform tmpParent = _obj.transform.parent;
		Quaternion tmpRot = _obj.transform.rotation;
		Vector3 tmpPos = _obj.transform.position;
		Vector3 tmpScale = _obj.transform.localScale;
		
		//Set to zero
		_obj.transform.SetParent (null);
		_obj.transform.rotation = Quaternion.identity;
		_obj.transform.position = Vector3.zero;
		_obj.transform.localScale = Vector3.one;
		
		//Calculate bounds now (in worl coordinates, becuase Unity gives me that)
		Bounds ret = GetBounds(_obj);
		
		//Restore original position and rotation
		_obj.transform.SetParent (tmpParent);
		_obj.transform.rotation = tmpRot;
		_obj.transform.position = tmpPos;
		_obj.transform.localScale = tmpScale;
		
		return ret;
	}
	
	public static void DoRecursively(this GameObject _obj, System.Action< GameObject > _action) {
		_action(_obj);
		foreach(Transform t in _obj.transform) {
			t.gameObject.DoRecursively(_action);
		}
	}
	
	public static void ChangeLayerRecursively(this GameObject _obj, int _layer) {
		_obj.DoRecursively( (_go) => {_go.layer = _layer;} );
	}

	public static T GetOrAddComponent<T> (this GameObject gameObject) where T : Component {
		T res = gameObject.GetComponent<T>();
		if (res == null)
		{
			res = gameObject.AddComponent<T>();
		}

		return res;
	}

	public static Component GetOrAddComponent (this GameObject gameObject, System.Type type) {
		Component res = gameObject.GetComponent(type);
		if (res == null)
		{
			res = gameObject.AddComponent(type);
		}

		return res;
	}
}
