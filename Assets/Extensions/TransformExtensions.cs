﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class TransformExtensions {
	
	public static void PositionOnParent(this Transform _transform, Transform _parent, bool _resetScale = false) {
		_transform.SetParent(_parent.transform);
		_transform.localPosition = Vector3.zero;
		_transform.localRotation = Quaternion.identity;
		if (_resetScale) {
			_transform.localScale = Vector3.one;
		}

		_transform.gameObject.ChangeLayerRecursively(_parent.gameObject.layer);
	}

	public static Transform[] FindChildren (this Transform _t, string _name)
	{
		return  _t.Cast<Transform> ().Where (t => t.name == _name).ToArray ();
	}

	public static Transform FindRecursively(this Transform _t, string _name){
		Transform ret = _t.Find (_name);
		if (ret == null) {
			for (int i = 0; i < _t.childCount && ret == null; ++i) {
				ret = _t.GetChild (i).FindRecursively (_name);
			}
		}
		return ret;
	}

	public static Transform FindParentRecursively(this Transform _t, string _name) {
		Transform res = null;
		Transform p = _t.parent;
		while (p != null) {
			if (p.name == _name) {
				res = p;
				break;
			}
			p = p.parent;
		}

		return res;
	}
}
