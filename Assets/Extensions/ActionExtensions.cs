﻿using System;
using UnityEngine;
using System.Collections;


public static class ActionExtensions {
	
	public static void SafeInvoke(this System.Action _action) {
		if(_action != null) {
			_action();
		}
	}
	
	public static void SafeInvoke< T >(this System.Action< T > _action, T p1) {
		if(_action != null) {
			_action(p1);
		}
	}
	
	public static void SafeInvoke< T1, T2 >(this System.Action< T1, T2 > _action, T1 p1, T2 p2) {
		if(_action != null) {
			_action(p1, p2);
		}
	}
	
	public static void SafeInvoke< T1, T2, T3 >(this System.Action< T1, T2, T3 > _action, T1 p1, T2 p2, T3 p3) {
		if(_action != null) {
			_action(p1, p2, p3);
		}
	}
	
	public static void SafeInvoke< T1, T2, T3, T4 >(this System.Action< T1, T2, T3, T4 > _action, T1 p1, T2 p2, T3 p3, T4 p4) {
		if(_action != null) {
			_action(p1, p2, p3, p4);
		}
	}
	
	public static void SafeCallbackInvoke(this System.Action<  System.Action > _action, System.Action callback) {
		if(_action != null) {
			_action(callback);
		}
		else callback.SafeInvoke();
	}
	
	public static void SafeCallbackInvoke< T >(this System.Action< T, Action > _action, T p1, System.Action callback) {
		if(_action != null) {
			_action(p1, callback);
		}
		else callback.SafeInvoke();
	}
	
	public static void SafeCallbackInvoke< T1, T2 >(this System.Action< T1, T2, Action > _action, T1 p1, T2 p2, System.Action callback) {
		if(_action != null) {
			_action(p1, p2, callback);
		}
		else callback.SafeInvoke();
	}
	
	public static void SafeCallbackInvoke< T1, T2, T3 >(this System.Action< T1, T2, T3, Action > _action, T1 p1, T2 p2, T3 p3, System.Action callback) {
		if(_action != null) {
			_action(p1, p2, p3, callback);
		}
		else callback.SafeInvoke();
	}

	
}

